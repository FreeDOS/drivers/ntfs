/*  attrtypes.h - define historical default values for attribute types
    Copyright 1999 by Steve Dodd <dirk@loth.demon.co.uk>

    This program is free software; you can redistribute it and/or modify
    it under the terms of version 2 of the GNU General Public License as
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See version 2
    of the GNU General Public License for more details.

    You should have received a copy of version 2 of the GNU General
    Public License along with this program; if not, write to the
    Free Software Foundation, Inc., 675 Mass Ave, Cambridge,
    MA 02139, USA.

    Version: $Id: attrtype.h,v 1.1.1.1 2001/02/11 16:12:51 kmaster Exp $
*/

#define ATTR_STANDARD_INFORMATION       0x10
#define ATTR_ATTRIBUTE_LIST             0x20
#define ATTR_FILE_NAME                  0x30
#define ATTR_VOLUME_VERSION             0x40
#define ATTR_SECURITY_DESCRIPTOR        0x50
#define ATTR_VOLUME_NAME                0x60
#define ATTR_VOLUME_INFORMATION         0x70
#define ATTR_DATA                       0x80
#define ATTR_INDEX_ROOT                 0x90
#define ATTR_INDEX_ALLOCATION           0xa0
#define ATTR_BITMAP                     0xb0
#define ATTR_SYMBOLIC_LINK              0xc0
#define ATTR_EA_INFORMATION             0xd0
#define ATTR_EA                         0xe0

#define AFLAG_INDEXABLE         0x1     /* may be indexed */
#define AFLAG_REGENERATE        0x40    /* needs to be regenerated */
#define AFLAG_NONRESIDENT       0x80    /* may be non-resident */

#define ASIZE_MIN ((ntfs_u64)0)
#define ASIZE_MAX (~((ntfs_u64)0))
