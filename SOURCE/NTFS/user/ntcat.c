/*
 *  ntcat.c
 *
 *  Copyright (C) 1995-1997 Martin von L�wis
 *  Copyright (C) 1997 R�gis Duchesne
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdio.h>
#include <stdlib.h>
#ifdef HAVE_GETOPT_H
#include <getopt.h>
#else
#define getopt_long(a,v,o,ol,x)        getopt(a,v,o)
#endif
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif
#ifdef _WINDOWS
#include <fcntl.h>
#endif
#ifdef HAVE_IO_H
#include <io.h>
#endif
#include <string.h>
#include "ntfstypes.h"
#include "struct.h"
#include "util.h"
#include "inode.h"
#include "nttools.h"

char *short_opts="o:s:B:b:1Vh";
#ifdef HAVE_GETOPT_H
struct option options[]={
	{"offset",1,0,'o'},
	{"size",1,0,'s'},
	{"bias",1,0,'B'},
	{"buflen",1,0,'b'},
	{"8859-1",0,0,'1'},
	{"version",0,0,'V'},
	{"help",0,0,'h'},
	{0,0,0,0}
};
#endif

char usage_str[]=
"ntcat: dumps the contents of an NTFS file to stdout\n"
"ntcat [OPTIONS] [directory/]filename\n"
"  --offset, -o offset	     Use offset\n"
"  --size, -s size           Only first size bytes\n"
"  --bias, -B bias           Use partition bias\n"
"  --buflen, -b buflen       Read with buffer size buflen\n"
"  --8859-1, -1              Use charset ISO-8859-1\n"
"                            (default is UTF-8)\n"
"  --version, -V             Display version\n"
"  --help, -h                Display this message\n"
;


void usage(void)
{
/*      fprintf(stderr,"ntcat [-f device] [-o offset] [-l size] [-1] [directory/]filename\n");*/
	fprintf(stderr,usage_str);

}

/* print the file ino on stdout */
void ntfs_cat_file(ntfs_inode *ino,int offset,int length,int bufsize)
{
	int position;
	char *buf;
	ntfs_io io;

	position=offset;
	if(!bufsize) bufsize=8192;
	buf=(char*)malloc(bufsize);
	/* Very large file */
	if(!buf)
	{
		bufsize=8192;
		buf=(char*)malloc(bufsize);
		if(!buf)
		{
			perror("ntcat");
			exit(0);
		}
	}
	io.fn_put=ntfs_put;
	io.fn_get=0;
	io.do_read=1;
	io.param=buf;
	io.size=bufsize;
	/* read from the unnamed data attribute */
	while(ntfs_read_attr(ino,ino->vol->at_data,
			     NULL,position,&io)==0 && io.size)
	{
		if(write(1,buf,io.size)!=io.size){
			perror("write");
			exit(1);
		}
		position+=io.size;
		length-=io.size;
		if(length==0)
			break;
		io.param=buf;
		io.size=bufsize;
	}
	free(buf);
}

int main(int argc,char *argv[])
{
	int c;
	char device[256]={'\0'};        /* KM */
	char *it;                       /* KM */
#ifdef _WINDOWS
	devname[32];
#endif
	int offset=0;
	int length=0;
	char *name;
	ntfs_volume *volume;
	ntfs_inode ino;
	int inum;
	int bias=0;
	int buflen=8192;
	extern int opterr,optind;
	extern char* optarg;
	unsigned int charset=nct_utf8;

	opterr=1;
	while((c=getopt_long(argc,argv,short_opts,options,NULL))>0)
		switch(c)
		{
		case 'o': offset=strtol(optarg,NULL,0);break;
		case 's': length=strtol(optarg,NULL,0);break;
		case 'B': bias=strtol(optarg,NULL,0);break;
		case 'b': buflen=strtol(optarg,NULL,0);break;
		case '1': charset=nct_iso8859_1;break;
		case 'V': printf("ntcat " NTFS_VERSION "\n");exit(0);break;
		case 'h': usage();exit(0);break;
		}
	if(optind+1!=argc){
		usage();
		exit(1);
	}
	name=argv[optind];
	for(it=name+2;*it && *it!='/';it++)
		/*nothing*/;
	if(it!=name+2)
	{
	  strcpy(device,"/dev/");
	  strncpy(device+5,name+2,(it-name)-2);
	  device[(it-name)+3]='\0';
	}
#ifdef _WINDOWS
	_setmode(fileno(stdout),_O_BINARY);
	if(!device && name[1]=':'){
		strcpy(devname,"\\\\.\\C:");
		devname[4] = name[0];
		name+=3;
		device=devname;
	}
#endif
	volume=ntfs_open_volume(device,bias,1,0);
	if(!volume)return 1;
	volume->nct=charset;
	inum=5;
	name=it;                /* KM */
	if(*name=='/')name++;   /* KM */
	/* walk the directory tree */
	do{
		char *next;
		if(ntfs_init_inode(&ino,volume,inum)){
			fprintf(stderr,"error finding %s\n",name);
			return 1;
		}
		if(!name || !*name)break;
		next=strpbrk(name,NTFS_PATH_SEP);
		if(next){
			*next='\0';
			next++;
		}
		inum=ntfs_find_file(&ino,name);
		if(inum==-1){
			fprintf(stderr,"%s not found\n",name);
			return 1;
		}
		name=next;
	}while(1);
	if(ntfs_init_inode(&ino,volume,inum))
		fprintf(stderr,"error opening %s\n",name);
	ntfs_cat_file(&ino,offset,length,buflen);
	return 0;
}

/*
 * Local variables:
 * c-file-style: "linux"
 * End:
 */
