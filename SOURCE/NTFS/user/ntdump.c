/*
 *  ntdump.c
 *
 *  Copyright (C) 1995-1997, 1999 Martin von L�wis
 *  Copyright (C) 1997 R�gis Duchesne
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "ntfstypes.h"
#include "struct.h"
#include "util.h"
#include "super.h"
#include "nttools.h"
#include "inode.h"
#include "dump.h"
#include <stdio.h>
#include <stdlib.h>
#ifdef HAVE_GETOPT_H
#include <getopt.h>
#else
#define getopt_long(a,v,o,ol,x)        getopt(a,v,o)
#endif
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif

char *short_opts="vrdf:o:c:Mi:ID:B:A:N:nhV";
#ifdef HAVE_GETOPT_H
struct option options[]={
	{"raw",0,0,'r'},
	{"filesystem",1,0,'f'},
	{"offset",1,0,'o'},
	{"cluster",1,0,'c'},
	{"mft",0,0,'M'},
	{"inode",1,0,'i'},
	{"dir",0,0,'d'},
	{"info",0,0,'I'},
	{"decompress",1,0,'D'},
	{"verbose",0,0,'v'},
	{"bias",1,0,'B'},
	{"attribute-type",1,0,'A'},
	{"attribute-name",1,0,'N'},
	{"linode",0,0,'n'},
	{"help",0,0,'h'},
	{"version",0,0,'V'},
	{0,0,0,0}
};
#endif

void usage(void)
{
	fprintf(stderr,"ntdump: prints low-level structures of an NTFS\n"
		"  --filesystem, -f device  Use device\n"
		"  --raw, -r                Access the raw device\n"
		"  --offset, -o n           Start at offset o\n"
		"  --cluster, -c n          Start at cluster n\n"
		"  --mft, -M                Display as master file table record\n"
		"  --inode, -i n            Display inode n\n"
		"  --dir, -d                Display as directory\n"
		"  --info, -I               Display file system information\n"
		"  --decompress, -D n       Decompress run n\n"
		"  --bias, -B n             Add partition bias n [bytes]\n"
		"  --attribute-type, -A n   Dump type n\n"
		"  --attribute-name, -N str Dump attribute named str\n"
		"  --verbose, -v            decompress verbose\n"
		"  --linode, -n             show internal inode representation\n"
		"  --version, -V            print version number\n"
		);
}

int main(int argc,char *argv[])
{
	int c,with_ino,error;
	int raw=0,as_mft=0,as_dir=0,use_ino=0,get_info=0;
	int decompress=0,verbose=0,as_inode=0;
	unsigned long inode = 0, cluster = 0, bias = 0;
	unsigned long attribute_type = 0x80; /* DATA */
	char *attribute_name=0;
	char *device=0;
	ntfs_size_t offset=0;
	ntfs_volume *volume;
	extern int optind,opterr;
	extern char *optarg;

	opterr=1;
	while((c=getopt_long(argc,argv,short_opts,options,NULL))>0)
		switch(c)
		{
		case 'r': raw=1;break;
		case 'f': device=optarg;break;
		case 'o': offset=strtoul(optarg,NULL,0);break;
		case 'c': cluster=strtoul(optarg,NULL,0);break;
		case 'M': as_mft=1;break;
		case 'i': inode=strtoul(optarg,NULL,0);use_ino=1;break;
		case 'd': as_dir=1;break;
		case 'I': get_info=1;break;
		case 'D': decompress=1;offset=strtoul(optarg,NULL,0);break;
		case 'v': verbose=1;break;
		case 'B': bias=strtoul(optarg,NULL,0);break;
		case 'A': attribute_type=strtoul(optarg,NULL,0);break;
		case 'N': attribute_name=optarg;break;
		case 'n': as_inode=1;break;
		case 'V': printf("ntdump " NTFS_VERSION "\n");exit(0);break;
		default:
			usage();
			exit(1);
		}
	with_ino = use_ino || get_info;
	volume=ntfs_open_volume(device,bias,0,!with_ino);
	if(!volume && !raw)return 1;
	if(!volume){
		extern ntfs_volume* the_vol;
		volume = the_vol;
	}
	if(cluster)
		offset=volume->clustersize*cluster;
	if(use_ino){
		ntfs_inode ino;
		ntfs_init_inode(&ino,volume,inode);
		if(as_mft){
			list_attr_mem(volume,ino.attr);
			return 0;
		}
		if(as_dir){
			dumpdir(&ino);
			return 0;
		}
		if(as_inode){
			dump_inode(&ino);
			return 0;
		}
		if(decompress){
			dump_decompress(&ino,offset,verbose);
			return 0;
		}
		while(1)
		{
			char buf[4096];
			ntfs_io io;

			io.fn_put=ntfs_put;
			io.fn_get=0;
			io.param=buf;
			io.size=volume->clustersize;
			error=ntfs_read_attr(&ino,attribute_type,
					     attribute_name,offset,&io);
			if(error)return 0;
			dump_mem(buf,offset,io.size);
			offset+=io.size;
			if(io.size<volume->clustersize)return 0;
		}
		return 0;
	}
	if(raw)
	{
		dump(volume,offset+bias,offset,-1);
	}else if(as_mft)
	{
		list_attributes(volume,offset);
	}else if(get_info)
	{
		long volsize;
		printf("Blocksize: %x\nClustersize: %x\nMFT size: %x\n",
		       volume->blocksize,volume->clustersize,
		       volume->mft_recordsize);
		if (ntfs_get_volumesize(volume, &volsize) == 0)
		    printf("Total clusters: %ld\n",volsize);
		printf("Free clusters:  %d\n",
		       ntfs_get_free_cluster_count(volume->bitmap));
	}else{
		usage();
	}
	return 0;
}

/*
 * Local variables:
 * c-file-style: "linux"
 * End:
 */
