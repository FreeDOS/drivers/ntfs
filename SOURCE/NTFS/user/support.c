/*
 *  support.c
 *  Specific support functions
 *
 *  Copyright (C) 1999 Martin von L�wis
 *  Copyright (C) 1997 R�gis Duchesne
 */

#include "ntfstypes.h"
#include "struct.h"
#include "support.h"
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif
#ifdef MSDOS
#include "../dos/readdisk.h"
#endif

#include <stdlib.h>
/* Solaris defines bzero() here */
#ifdef HAVE_STRINGS_H
#include <strings.h>
#endif
#ifdef HAVE_STRING_H
#include <string.h>
#endif
#ifdef HAVE_IO_H
#include <io.h>
#endif
#include <stdarg.h>
#include <stdio.h>
#include <errno.h>
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif
#include <time.h>
#include "inode.h"
#include "nttools.h"
#include "macros.h"
#include "util.h"

int ntdebug;

void ntfs_debug(int mask, const char *fmt, ...)
{
	va_list args;

	if (!(mask & ntdebug))
		return;
	va_start( args, fmt );
	vfprintf( stderr, fmt, args );
	va_end (args);
}

void *ntfs_malloc(int size)
{
    return malloc(size);
}

void ntfs_free(void *block)
{
    free(block);
}

void ntfs_bzero(void *s, int n)
{
#ifdef HAVE_BZERO
    bzero(s, n);
#else
#ifdef HAVE_MEMSET
	memset(s,'\0',n);
#else
#error no bzero implementation
#endif
#endif
}

void ntfs_memcpy(void *dest, const void *src, ntfs_size_t n)
{
	memcpy(dest, src, n);
}

void ntfs_memmove(void *dest, const void *src, ntfs_size_t n)
{
	memmove(dest, src, n);
}

/* Warn that an error occured */
void ntfs_error(const char *fmt,...)
{
	va_list ap;

	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);
	fputs("", stderr);
}

int ntfs_read_mft_record(ntfs_volume *vol, int mftno, char *buf)
{
	ntfs_io io;
	int error;

	io.fn_put=0;
	io.fn_get=0;
	/* record 0 of file 0 is always in memory */
	if(mftno==0)
	{
		memcpy(buf,vol->mft,vol->mft_recordsize);
		return 0;
	}
	if(!vol->mft_ino)
	{
		fprintf(stderr,"Cannot load mft %x without mft 0\n",mftno);
		return EINVAL;
	}
	io.param=buf;
	io.size=vol->mft_recordsize;
	error=ntfs_read_attr(vol->mft_ino,vol->at_data,NULL,
			   mftno*vol->mft_recordsize,&io);
	if(error)return error;
	if(io.size!=vol->mft_recordsize)return EINVAL;
	if(!ntfs_check_mft_record(vol,buf))
	{
		fprintf(stderr,"Inode not found\n");
		return EINVAL;
	}
	return 0;
}

/* pmemcpy is ignored here */
int ntfs_getput_clusters(ntfs_volume *pvol, int cluster, ntfs_size_t offs,
	ntfs_io *buf)
{
	int result;
	if(ntfs_lseek(NTFS_FD(pvol),
		      pvol->partition_bias+cluster*pvol->clustersize+offs,
		      SEEK_SET)==-1)
		return 0;
	/* MAGIC: We *know* at this place that we were originally passed a plain
	   pointer */
#ifdef MSDOS
	if(buf->do_read)
		result=read_dev(NTFS_FD(pvol),buf->param,buf->size);
	else
		result=write_dev(NTFS_FD(pvol),buf->param,buf->size);
#else
	if(buf->do_read)
		result=read(NTFS_FD(pvol),buf->param,buf->size);
	else
		result=write(NTFS_FD(pvol),buf->param,buf->size);
#endif
	if(result==buf->size){
		/* increment target pointer */
		((char*)buf->param)+=buf->size;
		return 0;
	}
	if(result==-1)return errno;
	return EIO;
}

ntfs_time64_t ntfs_now(void)
{
	return ntfs_unixutc2ntutc(time(0));
}

int ntfs_dupuni2map(ntfs_volume *vol, ntfs_u16 *in, int in_len, char **out,
	int *out_len)
{
	/* Not supported here */
	return EINVAL;
}

int ntfs_dupmap2uni(ntfs_volume *vol, char* in, int in_len, ntfs_u16 **out,
	int *out_len)
{
	/* Not supported here */
	return EINVAL;
}

/*
 * Local variables:
 * c-file-style: "linux"
 * End:
 */
