/*
 *  types.h
 *  This file defines four things:
 *   - generic platform independent fixed-size types (e.g. ntfs_u32)
 *   - specific fixed-size types (e.g. ntfs_offset_t)
 *   - macros that read and write those types from and to byte arrays
 *   - types derived from OS specific ones
 *
 *  Copyright (C) 1996,1998 Martin von L�wis
 */

#ifdef _MSC_VER
#include <nttypes.h>
#endif

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifdef HAVE_ENDIAN_H
#include <endian.h>
#else
#if defined(i386) || defined(__i386__)
#define __LITTLE_ENDIAN
#endif
#endif /* HAVE_ENDIAN */

/* integral types */
#ifndef NTFS_INTEGRAL_TYPES
#define NTFS_INTEGRAL_TYPES

#ifdef HAVE_INTTYPES_H

#include <inttypes.h>
typedef uint8_t  ntfs_u8;
typedef uint16_t ntfs_u16;
typedef uint32_t ntfs_u32;
typedef uint64_t ntfs_u64;
typedef int8_t ntfs_s8;
typedef int16_t ntfs_s16;
typedef int32_t ntfs_s32;
typedef int64_t ntfs_s64;

#else /* HAVE_INTTYPES_H */

#if defined(i386) || defined(__i386__)
typedef unsigned char      ntfs_u8;
typedef unsigned short     ntfs_u16;
typedef unsigned int       ntfs_u32;
typedef unsigned long long ntfs_u64;
typedef char      ntfs_s8;
typedef short     ntfs_s16;
typedef int       ntfs_s32;
typedef long long ntfs_s64;
#endif

#endif /*HAVE_INTTYPES_H */
#endif /*NTFS_INTEGRAL_TYPES */

#if defined(i386) || defined(__i386__)
/* unicode character type */
#ifndef NTFS_WCHAR_T
#define NTFS_WCHAR_T
typedef unsigned short     ntfs_wchar_t;
#endif
/* file offset */
#ifndef NTFS_OFFSET_T
#define NTFS_OFFSET_T
typedef unsigned long long ntfs_offset_t;
#endif
/* UTC */
#ifndef NTFS_TIME64_T
#define NTFS_TIME64_T
typedef unsigned long long ntfs_time64_t;
#endif
/* This is really unsigned long long. So we support only volumes up to 2 TB */
#ifndef NTFS_CLUSTER_T
#define NTFS_CLUSTER_T
typedef unsigned int ntfs_cluster_t;
#define MAX_CLUSTER_T (~((ntfs_cluster_t)0))
#endif

#else
#error Put your machine description here
#endif

#include "ntfsendian.h"

/* architecture independent macros */

/* PUTU32 would not clear all bytes */
#define NTFS_PUTINUM(p,i)    NTFS_PUTU64(p,i->i_number);\
                             NTFS_PUTU16(((char*)p)+6,i->sequence_number)

/* system dependent types */
#ifdef HAVE_SYS_TYPES
#ifndef NTMODE_T
#define NTMODE_T
typedef mode_t ntmode_t;
#endif
#ifndef NTFS_UID_T
#define NTFS_UID_T
typedef uid_t ntfs_uid_t;
#endif
#ifndef NTFS_GID_T
#define NTFS_GID_T
typedef gid_t ntfs_gid_t;
#endif
#ifndef NTFS_SIZE_T
#define NTFS_SIZE_T
typedef size_t ntfs_size_t;
#endif
#ifndef NTFS_TIME_T
#define NTFS_TIME_T
typedef time_t ntfs_time_t;
#endif
#else
#ifndef _MSC_VER
#include <sys/types.h>
#include <sys/time.h>
#include <sys/stat.h>
typedef mode_t ntmode_t;
typedef uid_t ntfs_uid_t;
typedef gid_t ntfs_gid_t;
typedef size_t ntfs_size_t;
typedef time_t ntfs_time_t;
#endif /* _MSC_VER */
#endif /* __linux__ */

/*
 * Local variables:
 * c-file-style: "linux"
 * End:
 */
