/*
 *  ntmkdir.c
 *
 *  Copyright (C) 1998 Martin von L�wis
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdio.h>
#include <ctype.h>
#ifdef HAVE_GETOPT_H
#include <getopt.h>
#else
#define getopt_long(a,v,o,ol,x)        getopt(a,v,o)
#endif
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif
#include <stdlib.h>
#include <string.h>
#include "ntfstypes.h"
#include "struct.h"
#include "util.h"
#include "support.h"
#include "nttools.h"
#include "dir.h"
#include "inode.h"

char *short_opts="f:hV";
#ifdef HAVE_GETOPT_H
struct option options[]={
	{"filesystem",1,0,'f'},
	{"version",0,0,'V'},
	{0,0,0,0}
};
#endif

char usage_str[]=
"ntmkdir: Creates a new directory\n"
"ntmkdir [OPTIONS] path\n"
"  --filesystem, -f device   Use device\n"
"  --help, -h                Display this message\n"
"  --version, -v             Display version\n"
;


void usage(void)
{
	fprintf(stderr,usage_str);
}


int main(int argc,char *argv[])
{
	int c;
	char *device=0;
	char *name;
	ntfs_volume *volume;
	ntfs_inode ino,new;
	int inum,bias=0;
	unsigned int type=ngt_nt;
	unsigned int charset=nct_utf8;
	extern int opterr,optind;
	extern char* optarg;
	int error;

	opterr=1;
	while((c=getopt_long(argc,argv,short_opts,options,NULL))>0)
		switch(c)
		{
		case 'f': device=optarg;break;
		case 'h': usage();exit(0);break;
		case 'V': printf("ntmkdir " NTFS_VERSION "\n");exit(0);break;
		default: usage();exit(1);
		}
	name=argv[optind];
	if(!name){
		usage();
		exit(1);
	}
	volume=ntfs_open_volume(device,bias,1,0);
	if(!volume)return 1;
	volume->ngt=type;
	volume->nct=charset;
	/* walk the directory */
	inum=5;
	do{
		char *next;
		if(ntfs_init_inode(&ino,volume,inum))
			fprintf(stderr,"error opening %s\n",name);
		next=strpbrk(name,NTFS_PATH_SEP);
		if(!next)
			break;
		*next='\0';
		next++;
		inum=ntfs_find_file(&ino,name);
		if(inum==-1){
			printf("%s not found\n",name);
			return 1;
		}
		name=next;
	}while(1);
	error = ntfs_mkdir(&ino,name,strlen(name),&new);
	if (error)
		printf("%s: %s\n",name,strerror(error));
	return 0;
}

/*
 * Local variables:
 * c-file-style: "linux"
 * End:
 */
