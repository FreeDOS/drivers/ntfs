/*
 *  dump.h
 *  Header file for dump.c
 *
 *  Copyright (C) 1997 R�gis Duchesne
 */

void list_attr_mem(ntfs_volume *vol, char *rec);
void dumpdir(ntfs_inode *ino);
void dump_inode(ntfs_inode *ino);
void dump_decompress(ntfs_inode *ino, int run, int verbose);
void dump_mem(unsigned char *buf, int start, int length);
void dump(ntfs_volume *vol, ntfs_size_t position, int start, int length);
void list_attributes(ntfs_volume *vol, ntfs_size_t offset);
int grep(ntfs_volume *vol, ntfs_size_t position, int length,
  unsigned char *string, int stringlen, int ignore_case);
