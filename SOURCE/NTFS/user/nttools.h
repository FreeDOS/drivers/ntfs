/*
 *  nttools.h
 *  Header file for nttools.c
 *
 *  Copyright (C) 1997 R�gis Duchesne
 */

void print_time(ntfs_time64_t t);
ntfs_volume *ntfs_open_volume(char *file, int bias, int silent, int no_inodes);
int ntfs_find_file(ntfs_inode *ino, char *name);
ntfs_size_t ntfs_lseek(int fd, ntfs_size_t offset, int whence);
void uniprint(char *first, int length);

#ifdef _WINDOWS
#define NTFS_PATH_SEP       "/\\"
#else
#define NTFS_PATH_SEP       "/"
#endif
#define PART_TYPE 0x7
