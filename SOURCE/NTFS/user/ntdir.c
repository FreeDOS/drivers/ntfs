/*
 *  ntdir.c
 *
 *  Copyright (C) 1995-1997 Martin von L�wis
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdio.h>
#include <ctype.h>
#ifdef HAVE_GETOPT_H
#include <getopt.h>
#else
#define getopt_long(a,v,o,ol,x)        getopt(a,v,o)
#endif
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif
#include <stdlib.h>
#include <string.h>
#include "ntfstypes.h"
#include "struct.h"
#include "util.h"
#include "support.h"
#include "nttools.h"
#include "dir.h"
#include "inode.h"

char *short_opts="lB:hUdnp1V";
#ifdef HAVE_GETOPT_H
struct option options[]={
	{"long",0,0,'l'},
	{"dos",0,0,'d'},
	{"nt",0,0,'n'},
	{"posix",0,0,'p'},
	{"bias",1,0,'B'},
	{"help",0,0,'h'},
	{"unsorted",0,0,'U'},
	{"8859-1",0,0,'1'},
	{"version",0,0,'V'},
	{0,0,0,0}
};
#endif

char usage_str[]=
"ntdir: displays the contents of a NTFS directory\n"
"ntdir [OPTIONS] path\n"
"  --dos, -d                 Display only short names\n"
"  --nt, -n                  Display only long names (default)\n"
"  --posix, -p               Display all but hidden files\n"
"  --long, -l                Display all files\n"
"  --unsorted, -U            Do not sort names alphabetically\n"
"  --8859-1, -1              Display names using ISO-8859-1\n"
"                            (default is UTF-8)\n"
"  --help, -h                Display this message\n"
;


void usage(void)
{
	fprintf(stderr,usage_str);
}

int ntfs_printcb(ntfs_u8 *entry,void *param)
{
	ntfs_volume *vol=(ntfs_volume*)param;
	int flags=NTFS_GETU8(entry+0x51);
	int show_hidden=0,to_lower=0;
	char *name;
	int name_len,i;
	switch(vol->ngt){
	case ngt_dos:
		if((flags & 2) == 0)
			return 0;
		break;
	case ngt_nt:
		if(flags==2)
			return 0;
		if((flags & 2) == 2)
			to_lower=1;
		break;
	case ngt_posix:
		to_lower=1;
		break;
	case ngt_full:
		show_hidden=1;
		break;
	}
	if(!show_hidden && ((NTFS_GETU8(entry+0x48) & 2) == 2))
		return 0;
	/* last access time? */
	print_time(NTFS_GETU64(entry+0x20));
	/* file size and mft record number */
	printf(" %10d %5d ",NTFS_GETU32(entry+0x40),NTFS_GETU32(entry));
	if(ntfs_encodeuni(vol,(ntfs_u16*)(entry+0x52),NTFS_GETU8(entry+0x50),
			   &name,&name_len))
	{
		printf("(name print error)\n");
		return 0;
	}
	if(to_lower)
		for(i=0;i<name_len;i++)
			putchar(tolower(name[i]));
				
	else
		printf("%s",name);
	free(name);
	putchar('\n');
	return 0;
}

/* display the directory ino */
void ntfs_print_dir(ntfs_inode* ino)
{
	ntfs_u64 fileno;
	int first;
	char item[1000];
	ntfs_iterate_s walk;
	walk.type=BY_POSITION;
	walk.dir=ino;
	walk.u.pos=0;
	walk.result=item;
	for(fileno=0,first=1;1;)
	{
		/* get the next entry */
		if(!ntfs_getdir_byposition(&walk))return;
		/* on the first iteration, we won't get any data */
		if(first){
			first=0;
			continue;
		}
		ntfs_printcb(item,ino->vol);
	}
}

void ntfs_print_dir_unsorted(ntfs_inode* ino)
{
	ntfs_u32 ph=0,pl=0;
	int error;
	while(1){
		if((error=ntfs_getdir_unsorted(ino,&ph,&pl,
					       ntfs_printcb,ino->vol)))
			fprintf(stderr,"Dir error %x\n",error);
		if(ph==0xFFFFFFFF)
			return;
	}
}

/* find file name in directory ino, return MFT record number if found */
int main(int argc,char *argv[])
{
	int c;
	char device[256]={'\0'};
	char *it;
	char *name;
	ntfs_volume *volume;
	ntfs_inode ino;
	int inum,bias=0;
	unsigned int type=ngt_nt;
	unsigned int charset=nct_utf8;
	int unsorted=0;
	extern int opterr,optind;
	extern char* optarg;

	opterr=1;
	while((c=getopt_long(argc,argv,short_opts,options,NULL))>0)
		switch(c)
		{
		case 'd': type=ngt_dos;break;
		case 'n': type=ngt_nt;break;
		case 'p': type=ngt_posix;break;
		case 'l': type=ngt_full;break;
		case 'B': bias=strtol(optarg,NULL,0);break;
		case 'h': usage();exit(0);break;
		case 'U': unsorted=1;break;
		case '1': charset=nct_iso8859_1;break;
		case 'V': printf("ntdir " NTFS_VERSION "\n");exit(0);break;
		default: usage();exit(1);
		}
	if(optind+1!=argc){
		usage();
		exit(1);
	}
	name=argv[optind];
	for(it=name+2;*it && *it!='/';it++)
		/*nothing*/;
	if(it!=name+2)
	{
	  strcpy(device,"/dev/");
	  strncpy(device+5,name+2,(it-name)-2);
	  device[(it-name)+3]='\0';
	}
	volume=ntfs_open_volume(device,bias,1,0);
	if(!volume)return 1;
	volume->ngt=type;
	volume->nct=charset;
	/* walk the directory */
	inum=5;
	name=it;
	if(*name=='/')name++;
	do{
		char *next;
		if(ntfs_init_inode(&ino,volume,inum))
			fprintf(stderr,"error opening %s\n",name);
		if(!name || !*name)break;
		next=strpbrk(name,NTFS_PATH_SEP);
		if(next){
			*next='\0';
			next++;
		}
		inum=ntfs_find_file(&ino,name);
		if(inum==-1){
			printf("%s not found\n",name);
			return 1;
		}
		name=next;
	}while(1);
	if(unsorted)
		ntfs_print_dir_unsorted(&ino);
	else
		ntfs_print_dir(&ino);
	return 0;
}

/*
 * Local variables:
 * c-file-style: "linux"
 * End:
 */
