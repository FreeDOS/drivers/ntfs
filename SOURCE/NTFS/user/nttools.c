/*
 *  nttools.c
 *  Helper functions for the tools
 *
 *  Copyright (C) 1995-1997, 1999 Martin von L�wis
 *  Copyright (C) 1997 R�gis Duchesne
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "ntfstypes.h"
#include "struct.h"
#include "nttools.h"

#include <stdio.h>
#ifdef HAVE_FCNTL_H
#include <fcntl.h>
#endif
#ifdef HAVE_IO_H
#include <io.h>
#endif
#include <errno.h>
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif
#include <sys/stat.h>
#include <sys/types.h>
#include <stdarg.h>
#include <time.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include "super.h"
#include "inode.h"
#include "dir.h"
#include "macros.h"
#include "support.h"
#include "util.h"
#ifdef MSDOS
#include "../dos/readdisk.h"
#endif

ntfs_size_t ntfs_lseek(int fd, ntfs_size_t offset, int whence)
{
#ifdef __linux__
	/* Linux should have llseek, although glibc does not have a prototype */
	loff_t llseek(int,loff_t, int);
	return llseek(fd,(loff_t)offset,whence);
#elif defined MSDOS
	return lseek_dev(fd,(off_t)offset,whence);
#else
	return lseek(fd,(off_t)offset,whence);
#endif
}

void ntfs_release_cluster(void *data)
{
	free(data);
}

int open_volume(char *name)
{
	/* binary for win32 support */
#ifndef O_BINARY
#define O_BINARY 0
#endif
#ifdef MSDOS
	int fd=open_dev(name,O_RDWR|O_BINARY);
#else
	int fd=open(name,O_RDWR|O_BINARY);
#endif
	if(fd==-1 && errno==EACCES){
		fprintf(stderr,"RW denied, trying RO\n");
#ifdef MSDOS
		fd=open_dev(name,O_RDONLY);
#else
		fd=open(name,O_RDONLY);
#endif
	}
	if(fd==-1)
	{
		perror("open");
		exit(0);
	}
	return fd;
}

ntfs_volume *the_vol;

ntfs_volume *ntfs_open_volume(char *file, int bias, int silent, int no_inodes)
{
	int fd;
	char cluster0[512];
	ntfs_io io;
	ntfs_volume *newvol;
	char *debug;

	/* Set debugging level. */
	debug = getenv ("NTFSDEBUG");
	if (debug) {
		extern int ntdebug;
		ntdebug = atoi (debug);
	}

	io.fn_put=0;
	io.fn_get=0; /* we don't need copy functions here */
	newvol=malloc(sizeof(ntfs_volume));
	newvol->partition_bias=bias;
#ifdef NTFS_VOLUME
	NTFS_FD(newvol)=fd=open_volume(file=file?file:NTFS_VOLUME);
#else
	if(!file){
		fprintf(stderr,"No volume specified or configured\n");
		return 0;
	}
	NTFS_FD(newvol)=fd=open_volume(file);
#endif
	if(fd<0)
	{       
		return 0;
	}
	/* read the boot sector */
	io.do_read=1;
	io.param=cluster0;
	io.size=512;
	ntfs_getput_clusters(newvol,0,0,&io);
	if(!IS_NTFS_VOLUME(cluster0)){
		fprintf(stderr,"Not a NTFS volume:%s\n",file);
		the_vol = newvol;
		the_vol->clustersize = 512;
		return 0;
	}
	ntfs_init_volume(newvol,cluster0);
	/* read the first mft record */
	newvol->mft=malloc(newvol->mft_recordsize);
	io.do_read=1;
	io.param=newvol->mft;
	io.size=newvol->mft_recordsize;
	ntfs_getput_clusters(newvol,newvol->mft_cluster,0,&io);
	/* fix it */
	if(!ntfs_check_mft_record(newvol,newvol->mft)){
		fprintf(stderr,"MFT record not at cluster 0x%X\n",newvol->mft_cluster);
		return 0;
	}
	if(!silent)
		fprintf(stderr,"MFT record at block 0x%X, offset 0x%X\n",newvol->mft_cluster,
			newvol->mft_cluster*newvol->clustersize);
	if(!no_inodes)
		ntfs_load_special_files(newvol);
	return newvol;
}

/* print a unicode string */
void uniprint(char *first, int length)
{
	while(length--){
		putchar(*first++);
		/* character above 255 or not a valid unicode string */
		if(*first++){
			printf("!!!!Error printing file name\n");
			return;
		}
	}
}

void uniprint_lower(char *first,int length)
{
	while(length--){
		putchar(tolower(*first));
		first++;
		/* character above 255 or not a valid unicode string */
		if(*first++){
			printf("!!!!Error printing file name\n");
			return;
		}
	}
}

/* unit of 100ns since 1.1.1601 */
void print_time(ntfs_time64_t t)
{
	ntfs_time64_t sec;
	ntfs_time_t unix_utc;
	char *str;
	sec=t/10000000;
	unix_utc=sec-((ntfs_time64_t)369*365+89)*24*3600;
	str=ctime(&unix_utc);
	/* remove \n */
	str[strlen(str)-1]='\0';
	printf("%s",str);
}

/* answer the MFT record number for file name in directory ino */
int ntfs_find_file(ntfs_inode *ino, char *name)
{
	char item[1000];
	ntfs_iterate_s walk;
	int error=ntfs_decodeuni(ino->vol,name,strlen(name),
				 &walk.name,&walk.namelen);
	if(error)
		return -1;
	walk.type=BY_NAME;
	walk.dir=ino;
	walk.result=item;
	if(!ntfs_getdir_byname(&walk))return -1;
	free(walk.name);
	return *(int*)item;
}

/*
 * Local variables:
 * c-file-style: "linux"
 * End:
 */
