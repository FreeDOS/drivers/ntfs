/*

    File: fnctdsk.h

    Copyright (C) 1998-2001  Christophe Grenier <grenier@nef.esiea.fr>
  
    This software is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.
  
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

 */

#define s_head(E) ((E)[1])
#define s_sect(E) (((E)[2]) & 0x3F)
#define s_cyl(E)  (((((E)[2]) & 0xC0)<<2) | ((E)[3]))
#define e_head(E) ((E)[5])
#define e_sect(E) (((E)[6]) & 0x3F)
#define os(E)     ((E)[4])
#define ind_boot(E) ((E)[0])
#define e_cyl(E)  (word)(((((E)[6]) & 0xC0)<<2) | ((E)[7]))

dword CHS2SR(t_param_disk_cst disk_car,const word, const byte, const byte);
dword CHS2SRext(t_param_disk_cst disk_car,const uint, const uint, const uint);
word byte_per_sector(t_sector_cst);
uint sect_per_cluster(t_sector_cst);
uint sect_reserv(t_sector_cst);
uint nbr_fats(t_sector_cst);
uint nbr_rootdir(t_sector_cst);
dword nbr_sectB1(t_sector_cst);
uint media_descr(t_sector_cst);
word sect_per_FAT(t_sector_cst);
dword sect_per_FAT32(t_sector_cst);
word sect_per_track(t_sector_cst);
word nbr_side(t_sector_cst);
dword hidden_sect(t_sector_cst);
dword nbr_sectB2(t_sector_cst);
dword NT_sect(t_sector_cst);
byte NT_clustersize(t_sector_cst);

int bootable(t_entree_cst E);
dword sect_rel(t_entree_cst);
dword nbr_sect(t_entree_cst);
dword get_sect_s(t_param_disk_cst disk_car,t_entree_cst);
dword get_sect_e(t_param_disk_cst disk_car, t_entree_cst);
void fill_t_diskext(t_diskext, t_disk_cst, t_disk_cst,
    const unsigned int,const dword);
int test_over(t_param_disk_cst disk_car,t_entree_cst, t_entree_cst);
dword debut_abs_entree(t_param_disk_cst disk_car,t_entree_cst, t_disk_cst);
dword debut_rel_entree(t_entree_cst);
dword fin_abs_entree(t_param_disk_cst disk_car,t_entree_cst, t_disk_cst);
dword fin_rel_entree(t_entree_cst);
dword CHS2SR2(t_param_disk_cst disk_car,const t_CHS*CHS);
dword get_SR(t_param_disk_cst disk_car,t_disk_cst);
dword get_SR_part(t_param_disk_cst disk_car,t_diskext_cst);
dword get_SR_param_disk(t_param_disk_cst);
dword get_LBA(t_param_disk_cst disk_car,t_disk_cst);
dword get_LBA_part(t_param_disk_cst disk_car,t_diskext_cst);
dword get_LBA_param_disk(t_param_disk_cst);
t_disk set_pos(t_param_disk_cst disk_car,t_disk, dword);
t_diskext set_posexts(t_param_disk_cst disk_car,t_diskext, dword);
t_diskext set_posexte(t_param_disk_cst disk_car,t_diskext, dword);
void disk_avant(t_param_disk_cst disk_car,t_disk);
void disk_apres(t_param_disk_cst disk_car,t_disk);
void disk_avant(t_param_disk_cst disk_car,t_disk);
void disk_apres_ext(t_param_disk_cst disk_car,t_diskext);
t_disk entree_s2pos(t_param_disk_cst disk_car,t_entree_cst, t_disk_cst, t_disk);
t_disk entree_e2pos(t_param_disk_cst disk_car,t_entree_cst, t_disk_cst, t_disk);
void partition2entree(t_param_disk_cst disk_car,dword, t_diskext_cst, t_entree *);
void build_entree(t_param_disk_cst disk_car,t_disk_cst, t_disk_cst, t_disk_cst, uchar, t_entree*);
int entree2partition(t_param_disk_cst disk_car,dword pos,t_diskext partition, t_entree_cst entree, int status);
const char* errmsg_entree2partition(int errcode);
int read_MBR(t_param_disk_cst disk_car,void *buffer);
int read_ext(t_param_disk_cst disk_car,void *buffer, const t_diskext partition);
int write_MBR(t_param_disk_cst disk_car,void *buffer);
int write_ext(t_param_disk_cst disk_car,void *buffer, const t_diskext partition);
int write_clean_table(t_param_disk_cst);
int write_MBR_code(t_param_disk_cst);
void dup_t_CHS(t_CHS * CHS_dest, const t_CHS * CHS_source);
int compare_CHS(const t_CHS *CHS1, const t_CHS *CHS2);
int compare2uint(uint a, uint b);
void set_pos2(t_param_disk_cst disk_car, t_CHS * CHS, const dword sect);
t_list_part add_new_partition(t_list_part old, t_list_part new);
t_list_part insert_new_partition(t_list_part old, t_list_part new);
t_list_part sort_list_part(t_list_part list_part);
int compar_diskext(const t_diskext a, const t_diskext b);
int check_list_part(t_list_part list_part);
