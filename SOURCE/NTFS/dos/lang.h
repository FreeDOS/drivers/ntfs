/*

    File: lang.h

    Copyright (C) 1998-2001  Christophe Grenier <grenier@nef.esiea.fr>
  
    This software is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.
  
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

 */

#define m_CMDLINE       "\n\nTestDisk command line :"
#define m_DUMP_HEXA     "Dump Hexa\n"
#define m_DUMP_ASCII    "\nDump ascii\n"
#define m_SYS           "\nSys="
#define m_READ_ERROR_AT "\nRead error at %u/%u/%u\n"
#define c_YES           'Y'
#define c_NO            'N'
#define m_ADD_PART_RERR "\nAdd_part: Read error"
#define m_BAD_NMARK     "\nPartition sector doesn't have the endmark 0xAA55\nRun fdisk /mbr and restart TestDisk\n"
#define m_NO_FREE_ENTREE "\nUnable to add partition, no free entree in table"
#define m_ASK_ADD       "\nDid you want to unerase the following partition :"
#define m_AFTER_PART    "\nSearch continue after this partition\nNo if you think this partition don't match"
#define m_CHOICE_YN     "\nChoice ? [Y/N] "
#define m_ADD_PART_WERR "\nAdd_part: Write error"
#define m_FREE_SPACE    "Free space between"
#define m_AND           "and"
#define m_FOUND_EXT_0   "\nFound an extended partition without partition"
#define m_FOUND_EXT_1   "\nFound an extended partition with partition"
#define m_PRB_EXT       "\nProblem with the extended partition"
#define m_LINUX_WARN    "\nLINUX SWAP-SPACE warning: size can be different than original"
#define m_BAD_S_CYL     "\nBad starting cylinder"
#define m_BAD_S_HEAD    "\nBad starting head"
#define m_BAD_S_SECT    "\nBad starting sector"
#define m_BAD_E_CYL     "\nBad ending cylinder"
#define m_BAD_E_HEAD    "\nBad ending head"
#define m_BAD_E_SECT    "\nBad ending sector"
#define m_END_BFR_START "\nPartition end < start !"
#define m_PART_BFR_MIN  "\nPartition starts before its lower limit"
#define m_PART_AFT_MAX  "\nPartition ends after its upper limit"
#define m_BAD_RS        "\nBad relative sector."
#define m_BAD_SCOUNT    "\nBad sector count."
#define m_CHKFAT_RERR   "check_FAT: Read error\n"
#define m_CHKFAT_BAD_JUMP       "check_FAT: Bad jump in FAT partition\n"
#define m_CHKFAT_BYTESECT       "check_FAT: Incorrect number of byte per sector\n"
#define m_CHKFAT_SECT_CLUSTER   "check_FAT: Bad number of sectors per cluster\n"
#define m_CHKFAT_SECTTRACK      "check_FAT: Incorrect number of sectors per track\n"
#define m_CHKFAT_SIDE   "check_FAT: Incorrect number of sides\n"
#define m_CHKFAT_NBRFAT "check_FAT: Bad number of FAT\n"
#define m_CHKFAT_ENTRY  "check_FAT: Bad number of entries in root dir\n"
#define m_CHKFAT_MEDIA  "check_FAT: Bad descriptor\n"
#define m_CHKFAT_SIZE   "check_FAT: Incorrect size of partition\n"
#define m_CHKFAT_RESERV "check_FAT: Incorrect number of reserved sectors\n"
#define m_CHKFAT_SECTPFAT "check_FAT: Incorrect number of sectors per FAT\n"
#define m_CHKFAT_BAD_HIDDEN "check_FAT: Incorrect numbers of hidden sectors\n"
#define m_ANALYSE_DIR_FAT_RERR "\ndir_entries: read error"
#define m_PART_UNUSED   "\nWarning : Unused partition must be fill with 0"
#define m_PART_MUSTNBOOT        "\nMust not be bootable"
#define m_PART_HEADER_EXT   "\t\t\t\t  Start       End       Sect Rel       Size\r Partition"
#define m_PART_HEADER   "     Partition\t\t\t\tStart      End      Sect Rel       Size\n"
#define m_GOD_MODE      "\nGod mode\n\n     Partition\t\t\t    Start        End        Size"
#define m_PART_RD_ERR   "\nPartition : Read error\n"
#define m_PART_WR_ERR   "\nPartition : Write error\n"
#define m_ONLY_ONE_DOS  "Partition may contain only one DOS FAT partition\n"
#define m_ONLY_ONE_EXT  "Partition may contain only one extended partition\n"
#define m_NO_OS2MB      "There are hidden partitions, but no OS2 MultiBoot.\n"
#define m_NO_BOOTABLE   "No partition is bootable\n"
#define m_ONLY1MUSTBOOT "Only one partition must be bootable\n"
#define m_PARTITION     "\nPartition"
#define m_PARTITIONS    "\nPartitions"
#define m_SAME_SPACE    "The following two partitions use the same hard disk space\n"
#define m_PRESS_KEY     "\n\nPress any key to continue"
#define m_LOG_ERR       "\nCan not create testdisk.log\n"
#define m_DRIVE         "Drive"
#define m_CHECKDATA     "\n- Check data"
#define m_IDENTIFICATION "\nIdentification problem of drive"
#define m_CHKSPACE      "\n\n- Check space used   (S)kip"
#define m_CHKSPACE_TST  "\n\n- Check space used (test mode)   (S)kip"
#define m_NO_ERR_FND    "\n => No error found"
#define m_SKIP          "\n =>skipped"
#define m_CHOOSE_PART_TYPE "Use arrows to change partition type"
#define m_CHOICE        "\nChoice ? "
#define m_ASK_PART      "\nChoose a partition (-1 to quit)"
#define m_ASK_STATUS    "\n0 Deleted\n1 Primary\n2 Extended\nChoice ? "
#define m_INTERNAL_ERROR "\nInternal error"
#define m_MALLOC_ERROR  "\nMemory allocation error"
#define m_FREE_ERROR    "\nFree memory allocation error"
#define m_GODMODE_WRITE "Write it ? [Y/N] "
#define m_WRITE_CLEAN_TABLE     "Clear the MBR partition table ? [Y/N] "
#define m_WRITE_MBR_CODE    "Write a new MBR code ? [Y/N] "
#define m_STRUCT_BAD    "Structure: Bad"
#define m_STRUCT_OK             "Structure: Ok "
#define m_MBR_ORDER             "MBR order"
#define m_MBR_ORDER_GOOD        "Partitions order: Ok "
#define m_MBR_ORDER_BAD "Partitions order: Bad"
#define m_NO_EXT_PART   "\nNo extended partition"
#define m_ROOT_CLUSTER_RERR	"\nroot_cluster: read error"
#define m_WARN_LBA	"\nWarning: check your disk access mode (LBA or not)"
#define m_Usage	"\nUsage: TestDisk [/liar] [/paranoid] [/log] [/debug] [/dump]"\
		"\n       TestDisk [/mbr] [/clean]" \
	    "\n" \
	    "\n/search       : search in partition" \
	    "\n[/fast|/slow] : fast(default) or slow partition recovery mode" \
	    "\n/paranoid     : scan each cylinder" \
	    "\n/log          : create a testdisk.log file" \
	    "\n" \
	    "\nTestDisk checks and recovers lost partitions" \
	    "\nIt works with :" \
	    "\n- FAT12, FAT16 <32M, FAT16 >32M, FAT32" \
	    "\n- extended partition" \
	    "\n- NTFS" \
	    "\n- LINUX ext2fs, LINUX swap" \
	    "\n- OS/2 MultiBoot (used in PartitionMagic)" \
	    "\n" \
	    "\nIf you have problems with TestDisk or bug reports, please contacte me.\n"
#define m_Copyright	"TestDisk 3.1alpha, Data Recovery Utility by Christophe GRENIER, January 30 2001" \
	 "\ngrenier@nef.esiea.fr" \
	 "\nhttp://www.esiea.fr/public_html/Christophe.GRENIER/" \
	 "\n"

