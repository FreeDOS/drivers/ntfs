/*

    File: hdaccess.h

    Copyright (C) 1998-2001  Christophe Grenier <grenier@nef.esiea.fr>
  
    This software is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.
  
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

 */

int hd_read(t_param_disk_cst disk_car,const uint, void*, t_disk_cst);
int hd_write(t_param_disk_cst disk_car,const uint, void*, t_disk_cst);
int hd_read2(t_param_disk_cst disk_car,const uint, void*, t_diskext_cst);
int hd_write2(t_param_disk_cst disk_car,const uint, void*, t_diskext_cst);
void free_dos_buffer(void);
int alloc_dos_buffer(void);
