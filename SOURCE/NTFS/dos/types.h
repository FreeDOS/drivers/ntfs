/*

    file: types.h

    Copyright (C) 1998-2001  Christophe GRENIER <grenier@nef.esiea.fr>
  
    this software is free software; you can redistribute it and/or modify
    it under the terms of the gnu general public license as published by
    the free software foundation; either version 2 of the license, or
    (at your option) any later version.
  
    this program is distributed in the hope that it will be useful,
    but without any warranty; without even the implied warranty of
    merchantability or fitness for a particular purpose.  see the
    gnu general public license for more details.
  
    you should have received a copy of the gnu general public license
    along with this program; if not, write to the free software
    foundation, inc., 675 mass ave, cambridge, ma 02139, usa.

 */

typedef char int8;
typedef short int int16;
typedef int int32;
typedef long long int int64;

typedef char __s8;
typedef short int __s16;
typedef int __s32;
typedef long long int __s64;

typedef unsigned char uint8;
typedef unsigned short int uint16;
typedef unsigned int uint32;
typedef unsigned long long int uint64;

typedef unsigned char __u8;
typedef unsigned short int __u16;
typedef unsigned int __u32;
typedef unsigned long long int _u64;

typedef unsigned char uchar;
typedef unsigned char byte;
typedef unsigned short int word;
typedef unsigned long int dword;

#if defined __DJGPP__
typedef unsigned int uint;
typedef uint8 u_int8_t;
typedef uint16 u_int16_t;
typedef uint32 u_int32_t;
typedef uint64 u_int64_t;
#else
#include <sys/types.h>
#endif

#include <sys/types.h>
