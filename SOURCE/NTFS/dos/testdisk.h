/*

    file: testdisk.h

    Copyright (C) 1998-2001  Christophe GRENIER <grenier@nef.esiea.fr>
  
    this software is free software; you can redistribute it and/or modify
    it under the terms of the gnu general public license as published by
    the free software foundation; either version 2 of the license, or
    (at your option) any later version.
  
    this program is distributed in the hope that it will be useful,
    but without any warranty; without even the implied warranty of
    merchantability or fitness for a particular purpose.  see the
    gnu general public license for more details.
  
    you should have received a copy of the gnu general public license
    along with this program; if not, write to the free software
    foundation, inc., 675 mass ave, cambridge, ma 02139, usa.

 */

enum { TRUST_YES, TRUST_NO, TRUST_AUTO};
int test_MBR(t_param_disk_cst disk_car,int debug);
int analyse_file(char *);
int search_free(t_disk_cst, t_disk_cst, t_disk_cst);
FILE* init_log(const char*filename,int argc, char**argv);
