#define HDPARM_BUF_SIZ 0x1A
#define HD_RW_BUF_SIZ 0x10

#define NBR_SECT	8
#define PARA_CMD   	 8
#define buffer_size 0x100

typedef unsigned char uchar;
typedef unsigned char byte;
typedef unsigned short int word;
typedef unsigned long int dword;
#if defined __DJGPP__
typedef unsigned short int uint;
#endif

typedef uchar t_sector[0x200];
typedef uchar t_entree[0x10];
typedef word n_bufferw[buffer_size];
typedef byte n_buffer[0x200];
typedef n_buffer *t_buffer;
typedef struct cellule_disk *t_disk;
typedef struct cellule_disk *t_param_disk;
typedef const t_param_disk t_param_disk_cst;
typedef const uchar t_sector_cst[0x200];
typedef const uchar t_entree_cst[0x10];
typedef const struct cellule_disk *t_disk_cst;

struct cellule_disk
 {
  byte disk;
  word cylinder;
  byte head;
  byte sector;
 };
typedef struct cellule_param t_param;
struct cellule_param
{
  word size;
  word info;
  dword cyl;
  byte head;
  byte sect;
  dword nbrsect1;
  dword nbrsect2;
  word  bps;
};
int hd_super(int, int, int, int, int, int, void*);
int hd_identify(t_param_disk);
int hd_parm(byte, void*);

void free_dos_buffer(void);
int alloc_dos_buffer(void);
