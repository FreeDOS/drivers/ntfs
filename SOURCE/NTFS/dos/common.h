/*

    File: common.h

    Copyright (C) 1998-2001  Christophe Grenier <grenier@nef.esiea.fr>
  
    This software is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.
  
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

 */
#define SECTOR_SIZE     0x200

#define MAX_CYLINDERS 65535
#define MAX_HEADS 255
#define MAX_SECTORS 63


#define NBR_PART_MAX    40
#define NBR_DISK_MAX	20
#define TAB_PART 0x1BE
/* PARTITION TYPE */
#define P_NO_OS         0x00
#define P_12FAT         0x01
#define P_16FAT         0x04
#define P_EXTENDED      0x05
#define P_16FATBD       0x06
#define P_NTFS          0x07
#define P_HPFS          0x07
#define P_OS2MB         0x0A
#define P_32FAT_1       0x0B
#define P_32FAT_2       0x0C
#define P_EXTENDX       0x0F
#define P_16FATH        0x14
#define P_16FATBDH      0x16
#define P_NTFSH         0x17
#define P_32FAT_1H      0x1B
#define P_32FAT_2H      0x1C
#define P_NETWARE		0x65
#define P_OLDLINUX      0x81
#define P_LINSWAP       0x82
#define P_LINSWAP2      0x82
#define P_LINUX         0x83
#define P_BSD           0xA5
#define P_BEOS          0xEB
enum { STATUS_DELETED, STATUS_PRIM, STATUS_PRIM_BOOT, STATUS_LOG, STATUS_EXT, STATUS_EXT_IN_EXT};
enum {BAD_SS, BAD_ES, BAD_SH, BAD_EH, BAD_EBS, BAD_RS, BAD_SC, BAD_EC,
  BAD_SCOUNT};
int doprintf(const char *, ...);
int ecrit_rapport(const char *, ...);
void ecrit_rapport_string(const char *string,const int max_length);

typedef struct cellule_disk_param *t_param_disk;
typedef const struct cellule_disk_param *t_param_disk_cst;

typedef struct cellule_disk *t_disk;
typedef const uchar t_sector_cst[0x200];
typedef uchar t_sector[0x200];
typedef uchar t_entree[0x10];
typedef const uchar t_entree_cst[0x10];
typedef const struct cellule_disk *t_disk_cst;
typedef struct cellule_diskext *t_diskext;
typedef struct cellule_diskext2 *t_diskext2;
typedef const struct cellule_diskext *t_diskext_cst;
typedef struct cellule_CHS t_CHS;

struct cellule_CHS
{
  uint cylinder;
  uint head;
  uint sector;
};

typedef struct cellule_list_part * t_list_part;
struct cellule_list_part
{
  t_diskext part;
  t_list_part prev;
  t_list_part next;
};

struct cellule_disk_param
 {
  int disk;
  t_CHS CHS;
#ifdef DJGPP
  int mode;
#else
  char *name;
#endif
 };

struct cellule_disk
 {
  byte disk;
  uint cylinder;
  uint head;
  uint sector;
 };


struct cellule_diskext
 {
  uint disk;
  t_CHS start;
  t_CHS end;
  uchar part_type;
  dword part_size;
  dword sect_rel;
  uint status;
  uint order;
  uint errcode;
 };


struct cellule_diskext2
 {
  uint disk;
  t_CHS start;
  t_CHS end;
  uchar part_type;
  dword part_size;
  dword sect_rel;
  uint status;
  uint order;
 };

typedef struct cellule_param t_param;
struct cellule_param
{
  word size;
  word info;
  dword cyl;
  byte head;
  byte sect;
  dword nbrsect1;
  dword nbrsect2;
  word  bps;
};

typedef struct str_fat t_fat_test;
struct str_fat
{
  dword offset;
  int f_fat12;
  int f_fat16;
  int f_fat32;
};

void dup_t_disk(t_disk, t_disk_cst);
void dup_t_diskext(t_diskext, t_diskext_cst);
int hd_identify(t_param_disk,int debug);
int ask_YN(const char*);
int ask_number(const char*, const int,const int);
int read_line(void);
char read_char(const char *);
void *MALLOC(size_t size);
void FREE(void *ptr);
char read_key(void);
char test_key(void);
uint up2power(uint number);
uint up2power_aux(uint number);
int hd_parse(t_param_disk param_disk[NBR_DISK_MAX],const int debug);
void my_sort(void *base, size_t nmemb, size_t size, int (*compar)(const void *, const void *));
