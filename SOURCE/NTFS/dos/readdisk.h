int open_dev(char*,int);
#if defined(__DJGPP__) || defined(LINUX)
off_t lseek_dev(int,off_t, int whence);
ssize_t read_dev(int, void*, size_t);
ssize_t write_dev(int, void*, size_t);
#elif defined(__WIN32__)
long lseek_dev(int,off_t, int whence);
long read_dev(int, void*, size_t);
long write_dev(int, void*, size_t);
#else
long lseek_dev(int,off_t, int whence);
long read_dev(int, void*, size_t);
long write_dev(int, void*, size_t);
#endif
