/* manually maintained config.h for NT */
#define HAVE_FCNTL_H
#define HAVE_IO_H
#define HAVE_STRING_H
#define HAVE_MEMSET
#define HAVE_GETOPT_H
#define NTFS_VERSION	"980127"
#define NAME_MAX 255
