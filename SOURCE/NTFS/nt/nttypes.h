#define NTFS_INTEGRAL_TYPES
typedef unsigned char      ntfs_u8;
typedef unsigned short     ntfs_u16;
typedef unsigned __int32   ntfs_u32;
typedef unsigned __int64   ntfs_u64;

typedef char      ntfs_s8;
typedef short     ntfs_s16;
typedef __int32   ntfs_s32;
typedef __int64   ntfs_s64;

#define NTFS_WCHAR_T
typedef unsigned __int16  ntfs_wchar_t;

#define NTFS_OFFSET_T
typedef unsigned __int64 ntfs_offset_t;

#define NTFS_TIME64_T
typedef unsigned __int64 ntfs_time64_t;

#define NTFS_CLUSTER_T
typedef unsigned __int64 ntfs_cluster_t;

#define EOPNOTSUPP 0x80000000	// not defined by MSVC
#define ENODATA    0x80000001

// just to keep the compiler quiet
	
#pragma warning(disable:4018)	// disable signed/unsigned mismatch warning
#pragma warning(disable:4244)
#pragma warning(disable:4761)
#pragma warning(disable:4133)

#define inline __inline			// inline is a C++ operator

#if defined(_M_IX86) && _M_IX86 >= 300
#define __i386__
#endif

typedef unsigned int ntmode_t;
typedef unsigned int ntfs_uid_t;
typedef unsigned int ntfs_gid_t;
typedef unsigned int ntfs_size_t;
typedef unsigned int ntfs_time_t;
